# Linux Server Guide

# Table of contents

- [Introduction](#introduction)
- [Server resource monitoring](#SRM)
    - [Processes monitoring](#PM)
    - [Network monitoring](#NM)
    - [Disk usage monitoring](#DM)
    - [Memory usage monitoring](#MM)


## Introduction <a name="introduction"></a>

This guide is written regarding server management of the Physis PEB team's server. In particular, this server is hosted in Geneva (Switzerland), run Ubuntu 23.10, and has 20 GB of space for OS and 250 GB of space for other data (mounted in two different disks).


# Server resources monitoring <a name="SRM"></a>

Here we report a brief list of great command to use in order to monitor resources of your server.

>ℹ️ Remember to use `Ctrl + c` to kill a running command!

>ℹ️ Use sudo only when strictly necessary!

Reference link: https://www.digitalocean.com/community/tutorials/how-to-use-top-netstat-du-other-tools-to-monitor-server-resources#step-1-how-to-view-running-processes-in-linux

## Processes monitoring <a name="PM"></a>

The basic command to list all the active processes is `top`. Having a fairly spartan interface, the suggestion is to use the more advanced `htop`, that basically is an improved top command.

> If `htop` is not installed in your system, simply use `sudo apt install htop`

htop allows some useful keyboard shortcut like:
- `M` to sort processess by memory usage
- `P` to sort processess by processor usage
- `k [PID]` to kill a process



## Network monitoring <a name="NM"></a>

A good command is `nethogs`. Basically, this command associate each running application with its network traffic. We can easily see the ingoing and outgoing speed for each item and at the end the total amount.

With nethogs we can use some shortcuts, like:
- `M` to change the unit displayed (kb/s -> kb -> b -> mb)
- `R` to sort by received traffic
- `S` to sort by sent traffic
- `Q` to quit

> If `nethogs` is not installed in your system, use `sudo apt install nethogs`

Another useful command is `netstat`.




[TODO]: Aggiungere questa parte




> If `netstat` is not installed in your system, use `sudo apt install net-tools`

## Disk usage monitoring <a name="DM"></a>

The basic command is `df -h --total`, in this way we can see the memory usage in human-readable format (`-h`) and look also the total spac amount (`--total`).

To obtain a more precize analysis of the disk space used we can use `ncdu`.

> If `ncdu` is not installed in your system, simply use `sudo apt install ncdu`

Here some shortcut for ncdu:
- Up and down arrow to select different folders
- Enter to open the selected folder
- Left arrow to open the parent folder ("go back")
- `n` to sort by name
- `s` to sort by size
- `g` to show percentage and/or graphics
- `i` to display informations about the selected folder
- `q` to quit

> To scan the full filesystem we can use `ncdu -x /`

## Memory usage monitoring <a name="MM"></a>

For memory usage monitoring of our server a great command is `free -m`, where the `-m` express the result in megabytes instead of bytes.

Otherwise, we can use the `vmstat -s M` command to have a more precise overview of momory usage (here `-s M` is for visualize the result in MB). We can also use `vmstat -s -S M` to have a more clear and concise output result.